export const config = {
	url: "http://localhost:7043/",
	maxFileSize: 100 * 10 ** 6,
	
	links: {
		contact: "mailto:kirbykevinson@outlook.com",
		source: "https://gitlab.com/kirbykevinson/n-share-client",
		tos: "/tos/"
	}
};
