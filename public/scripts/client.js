import {config} from "./config.js";

import * as openpgp from './openpgp.min.mjs';

class Client {
	constructor() {
		this.elements = {
			forms: document.querySelectorAll("form"),
			
			errorMessage: document.querySelector("#error-message"),
			
			uploadArea: document.querySelector("#upload-area"),
			
			fileName: document.querySelector("#file-name"),
			fileDescription: document.querySelector("#file-description"),
			filePassword: document.querySelector("#file-password"),
			fileDownloadsLeft: document.querySelector("#file-downloads-left"),
			finishButton: document.querySelector("#finish-button"),
			
			downloadLink: document.querySelector("#download-link"),
			deleteLink: document.querySelector("#delete-link"),
			
			deleteButton: document.querySelector("#delete-button"),
			
			downloadButton: document.querySelector("#download-button"),
			
			justiceId: document.querySelector("#justice-id"),
			justicePassword: document.querySelector("#justice-password"),
			justiceButton: document.querySelector("#justice-button"),
			
			justiceLink: document.querySelector("#justice-link"),
			justiceIp: document.querySelector("#justice-ip"),
			justiceUa: document.querySelector("#justice-ua"),
			justiceDate: document.querySelector("#justice-date"),
			
			contactLink: document.querySelector("#contact-link"),
			sourceLink: document.querySelector("#source-link"),
			tosLink: document.querySelector("#tos-link")
		};
		
		this.file = null;
		
		this.switches = {
			showLinks: null,
			onePunchMode: null
		};
		
		this.fileInfo = {
			id: null,
			magic: null,
			
			name: null,
			description: null,
			password: null,
			downloadsLeft: null,
			size: null
		};
		
		this.elements.contactLink.href = config.links.contact;
		this.elements.sourceLink.href = config.links.source;
		this.elements.tosLink.href = config.links.tos;
		
		for (const form of this.elements.forms) {
			form.addEventListener("submit", (event) => {
				event.preventDefault();
			});
		}
		
		this.elements.uploadArea.addEventListener("click", () => {
			const chooser = document.createElement("input");
			
			chooser.type = "file";
			
			chooser.addEventListener("change", () => {
				this.chooseFile(chooser.files[0]);
			});
			
			chooser.click();
		});
		this.elements.uploadArea.addEventListener("dragover", (event) => {
			event.preventDefault();
		});
		this.elements.uploadArea.addEventListener("dragleave", (event) => {
			event.preventDefault();
		});
		this.elements.uploadArea.addEventListener("drop", (event) => {
			event.preventDefault();
			
			if (event.dataTransfer.files.length > 0) {
				this.chooseFile(event.dataTransfer.files[0]);
			}
		});
		
		for (const link of [
			this.elements.downloadLink,
			this.elements.deleteLink,
			this.elements.justiceLink,
			this.elements.justiceIp,
			this.elements.justiceUa
		]) {
			link.addEventListener("focus", (event) => {
				event.target.select();
			});
		}
		
		this.elements.finishButton.addEventListener("click", () => {
			this.confirmUpload();
		});
		this.elements.deleteButton.addEventListener("click", () => {
			this.deleteFile();
		});
		this.elements.downloadButton.addEventListener("click", () => {
			this.downloadFile();
		});
		
		this.elements.justiceButton.addEventListener("click", () => {
			this.executeJustice();
		})
	}
	
	run() {
		const searchParams = new URLSearchParams(location.search);
		
		this.fileInfo.id = searchParams.get("id");
		this.fileInfo.magic = searchParams.get("magic");
		
		this.switches.showLinks = searchParams.get("show-links");
		this.switches.onePunchMode = searchParams.get("one-punch-mode");
		
		if (this.switches.onePunchMode) {
			this.showForm("execution-of-justice");
		} else if (
			this.fileInfo.id &&
			this.fileInfo.magic &&
			this.switches.showLinks
		) {
			this.showLinks();
		} else if (this.fileInfo.id && this.fileInfo.magic) {
			this.showDelete();
		} else if (this.fileInfo.id) {
			this.showDownload();
		} else {
			this.showForm("upload");
		}
	}
	
	error(message) {
		this.elements.errorMessage.innerText =
			message[0].toUpperCase() +
			message.slice(1);
		
		this.showForm("error");
		this.showForm = () => {};
		
		return null;
	}
	
	async sendEvent(event) {
		const requestJson = JSON.stringify(event);
		
		let response = null;
		
		try {
			response = await fetch(config.url, {
				method: "POST",
				body: requestJson
			});
		} catch (e) {
			return this.error("couldn't access the server");
		}
		
		const responseJson = await response.json();
		
		if (responseJson.type == "error") {
			return this.error(responseJson.message);
		}
		
		return responseJson;
	}
	
	showForm(name) {
		for (const form of this.elements.forms) {
			if (form.id == name) {
				form.hidden = false;
				
				try {
					form.querySelector("input, button").focus();
				} catch (e) {}
			} else {
				form.hidden = true;
			}
		}
	}
	
	async showLinks() {
		const dummyLink = document.createElement("a");
		
		dummyLink.href = ".";
		
		this.elements.downloadLink.value =
			dummyLink.href +
			`?id=${encodeURIComponent(this.fileInfo.id)}`;
		this.elements.deleteLink.value =
			dummyLink.href +
			`?id=${encodeURIComponent(this.fileInfo.id)}` +
			`&magic=${encodeURIComponent(this.fileInfo.magic)}`;
		
		this.showForm("links");
	}
	async showDelete() {
		await this.fetchFileInfo();
		
		this.renderFilePreview(document
			.querySelector("#delete")
			.querySelector(".file-holder")
		);
		
		this.showForm("delete");
	}
	async showDownload() {
		await this.fetchFileInfo();
		
		this.renderFilePreview(document
			.querySelector("#download")
			.querySelector(".file-holder")
		);
		
		this.showForm("download");
	}
	
	async fetchFileInfo() {
		const event = await this.sendEvent({
			type: "info",
			
			id: this.fileInfo.id
		});
		
		if (!event) {
			return;
		}
		
		if (
			typeof(event.name) != "string" ||
			typeof(event.description) != "string" ||
			typeof(event["downloads-left"]) != "number" ||
			typeof(event.size) != "number"
		) {
			console.log(event);
			
			return this.error("illegal event from server");
		}
		
		this.fileInfo.name = event.name;
		this.fileInfo.description = event.description;
		this.fileInfo.downloadsLeft = event["downloads-left"];
		this.fileInfo.size = event.size;
	}
	
	renderFilePreview(element) {
		function formatSize(size) {
			if (size == 0) {
				return "0 B";
			}
			
			const units = ["B", "kB", "MB", "GB", "TB"];
			
			const magnitude = Math.floor(Math.log(size) / Math.log(1000));
			
			return (
				size / Math.pow(1000, magnitude)
			).toFixed(2) + " " + units[magnitude];
		}
		
		element.innerHTML = `
			<div class="icon">📄</div>
			<div>
				<div class="name"></div>
				<div class="description"></div>
				<div class="metadata"></div>
			</div>
		`;
		
		const
			name = element.querySelector(".name"),
			description = element.querySelector(".description"),
			metadata = element.querySelector(".metadata");
		
		name.innerText = `${this.fileInfo.name}`;
		description.innerText = `${this.fileInfo.description}`;
		metadata.innerText =
			`${formatSize(this.fileInfo.size)} · ` +
			`${this.fileInfo.downloadsLeft} downloads left`;
	}
	
	async chooseFile(file) {
		function randomString() {
			const randomNumbers = Array.from(
				crypto.getRandomValues(new Uint8Array(32))
			);
			
			return btoa(randomNumbers
				.map(code => String.fromCharCode(code))
				.join("")
			);
		}
		
		if (file.size > config.maxFileSize) {
			alert("The file is too big to upload");
			
			return;
		}
		
		this.file = file;
		
		this.elements.fileName.value = file.name;
		
		this.fileInfo.magic = randomString();
		this.fileInfo.size = file.size;
		
		this.showForm("info");
	}
	async confirmUpload() {
		this.showForm("loading");
		
		this.fileInfo.name = this.elements.fileName.value;
		this.fileInfo.description = this.elements.fileDescription.value;
		this.fileInfo.password = this.elements.filePassword.value;
		this.fileInfo.downloadsLeft = Number(
			this.elements.fileDownloadsLeft.value
		);
		
		const reader = new FileReader();
		
		reader.addEventListener("error", () => {
			this.error("couldn't read the file");
		});
		
		reader.addEventListener("load", () => {
			this.uploadFile(reader.result);
		});
		
		reader.readAsArrayBuffer(this.file);
	}
	async deleteFile() {
		if (!confirm("Are you sure you want to delete it?")) {
			return;
		}
		
		const event = await this.sendEvent({
			type: "delete",
			
			id: this.fileInfo.id,
			magic: this.fileInfo.magic
		});
		
		if (!event) {
			return;
		}
		
		location.search = "";
	}
	async downloadFile() {
		const password = prompt("Enter the password");
		
		if (password == null) {
			return;
		}
		
		this.showForm("loading");
		
		const event = await this.sendEvent({
			type: "download",
			
			id: this.fileInfo.id
		});
		
		if (!event) {
			return;
		}
		
		if (typeof(event.data) != "string") {
			console.log(event);
			
			return this.error("illegal event from server");
		}
		
		let decryptedData;
		
		try {
			decryptedData = await this.decryptData(event.data, password);
		} catch (e) {
			return this.error("wrong password");
		}
		
		const blob = new Blob([decryptedData]);
		
		const dummyLink = document.createElement("a");
		
		dummyLink.href = URL.createObjectURL(blob);
		dummyLink.download = this.fileInfo.name;
		
		dummyLink.click();
		
		location.search = "";
	}
	async executeJustice() {
		const id = decodeURIComponent(this.elements.justiceId.value);
		
		const event = await this.sendEvent({
			type: "execution-of-justice",
			
			id: id,
			"admin-password": this.elements.justicePassword.value
		});
		
		if (!event) {
			return;
		}
		
		if (
			typeof(event.magic) != "string" ||
			typeof(event.ip) != "string" ||
			typeof(event.ua) != "string" ||
			typeof(event.date) != "string"
		) {
			console.log(event);
			
			return this.error("illegal event from server");
		}
		
		const dummyLink = document.createElement("a");
		
		dummyLink.href = ".";
		
		if (event.magic) {
			this.elements.justiceLink.value =
				dummyLink.href +
				`?id=${encodeURIComponent(id)}` +
				`&magic=${encodeURIComponent(event.magic)}`;
		}
		
		this.elements.justiceIp.value = event.ip;
		this.elements.justiceUa.value = event.ua;
		this.elements.justiceDate.value = event.date;
		
		this.showForm("justice-executed");
	}
	
	async uploadFile(data) {
		const encryptedData = await this.encryptData(
			data,
			this.elements.filePassword.value
		);
		
		const event = await this.sendEvent({
			type: "upload",
			
			data: encryptedData,
			magic: this.fileInfo.magic,
			
			name: this.fileInfo.name,
			description: this.fileInfo.description,
			"downloads-left": this.fileInfo.downloadsLeft,
			size: this.fileInfo.size
		});
		
		if (!event) {
			return;
		}
		
		location.search =
			`?id=${encodeURIComponent(event.id)}` +
			`&magic=${encodeURIComponent(this.fileInfo.magic)}` +
			`&show-links=1`;
	}
	
	async encryptData(data, password) {
		const dataArr = new Uint8Array(data);
		
		const message = await openpgp.createMessage({binary: dataArr});
		
		return await openpgp.encrypt({
			message: message,
			passwords: password + "?"
		});
	}
	async decryptData(data, password) {
		const message = await openpgp.readMessage({armoredMessage: data});
		
		const decrypted = await openpgp.decrypt({
			message: message,
			passwords: password + "?",
			format: "binary"
		});
		
		return decrypted.data;
	}
}

window.client = new Client();
window.openpgp = openpgp;

window.client.run();
