# THIS PIECE OF SOFTWARE IS NO LONGER DEVELOPED

N-Share is a simple temporary encrypted file sharing app built on JSON
over HTTP. The app was made as a college project and has the following
features:

* Uploading files (wow!)
* File descriptions
* AES-based client-side file encryption
* File expiration after a certain number of downloads
* Downloading files
* Deleting files
* IP and user agent logging in case the authorities aren't happy

## How to run

* Run or find an instance of [n-share-server].
* Configure the file `public/scripts/config.js` according to your
  needs.
* Serve the `public` folder via an HTTP server.
* Browse to the address of your HTTP server.

[n-share-server]: https://gitlab.com/kirbykevinson/n-share-server

## How to reimplement

See [the protocol reference].

[the protocol reference]: protocol.md
