## Apache License, Version 2.0 (`LICENSE_APL2`)

* `public/fonts/roboto.woff2` -
  [source](https://fonts.google.com/specimen/Roboto)
  > Copyright 2014 Google

## GNU Lesser General Public License, Version 3.0 or later (`LICENSE_LGPL3`)

* `public/scripts/openpgp.min.mjs` -
  [source](https://unpkg.com/browse/openpgp@5.0.0/dist/openpgp.min.mjs)
  > Copyright 2011-2021 OpenPGP.js developers

## GNU Affero General Public License, Version 3.0 or later (`LICENSE`)

* All other files
  > Copyright 2021 Kirby Kevinson
